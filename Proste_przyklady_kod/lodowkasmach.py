#!/usr/bin/env python

import rospy
import smach
import smach_ros
import time
import random

# stany maszyny:
class DojscieDoLodowki(smach.State):
    def __init__(self):
        # wymienienie statusow mozliwych na wyjsciu stanu
        smach.State.__init__(self, outcomes=['w_trakcie','sukces'])
        self.counter = 0

    def execute(self, userdata):
        rospy.loginfo('Executing state DojscieDoLodowki') 
        # opoznienie czasowe, zeby zaobserwowac przejscia w smach viewerze
        time.sleep(2)
        # na poczatku otrzymujemy status "w_trakcie", pozniej status "sukces"
        if self.counter < 3:
            self.counter += 1
            return 'w_trakcie'
        else:
            return 'sukces'

class CzyLodowkaOtwarta(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['zamknieta','otwarta'])

    def execute(self, userdata):
        rospy.loginfo('Executing state CzyLodowkaOtwarta')
        time.sleep(5)
        # losowanie, czy robot zastal lodowke zamknieta lub otwarta
        x = random.randint(1,2)
        if x==1:
            return 'zamknieta'
        if x==2:
            return 'otwarta'

class OtwarcieLodowki(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['otwarta'])

    def execute(self, userdata):
        rospy.loginfo('Executing state OtwarcieLodowki')
        time.sleep(5)
        return 'otwarta'

class WziecieNapojuZLodowki(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['wyjmowanie','wyjeto'])
        self.counter = 0

    def execute(self, userdata):
        rospy.loginfo('Executing state WziecieNapojuZLodowki')
        time.sleep(2)
        if self.counter < 3:
            self.counter += 1
            return 'wyjmowanie'
        else:
            return 'wyjeto'

        



# main
def main():
    rospy.init_node('smach_example_state_machine')

    # wywolanie maszyny stanow SMACH
    sm = smach.StateMachine(outcomes=['napoj_zostal_wyjety'])

    # uruchomienie "introspection server"
    sis = smach_ros.IntrospectionServer('server_name', sm, '/PRZYKLAD_LODOWKA')
    sis.start()

    # otwarcie kontenera
    with sm:
        # dodanie stanow do kontenera
        smach.StateMachine.add('DojscieDoLodowki', DojscieDoLodowki(), 
                               transitions={'w_trakcie':'DojscieDoLodowki', 
                                            'sukces':'CzyLodowkaOtwarta'})
        smach.StateMachine.add('CzyLodowkaOtwarta', CzyLodowkaOtwarta(), 
                               transitions={'zamknieta':'OtwarcieLodowki',
                                            'otwarta':'WziecieNapojuZLodowki'})
        smach.StateMachine.add('OtwarcieLodowki', OtwarcieLodowki(), 
                               transitions={'otwarta':'WziecieNapojuZLodowki'})
        smach.StateMachine.add('WziecieNapojuZLodowki', WziecieNapojuZLodowki(), 
                               transitions={'wyjmowanie':'WziecieNapojuZLodowki', 
                                            'wyjeto':'napoj_zostal_wyjety'})                                        

    # wykonanie planu maszyny stanow
    outcome = sm.execute()

    # oczekiwanie na wylaczenie aplikacji poprzez skrot klawiszowy ctrl+c
    rospy.spin()
    sis.stop()


if __name__ == '__main__':
    main()